package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"
)

var mutex sync.Mutex //互斥锁
func printer(str string) {
	//mutex.Lock()				//加锁
	//defer mutex.Unlock()		//解锁
	for _, ch := range str {
		fmt.Printf("%c", ch)
		time.Sleep(time.Millisecond * 300)
	}
}
func user1() {
	printer("hello ")
}
func user2() {
	printer("world")
}
func main() {
	fmt.Println(TmpFileAssignHelper(0, "main/mr-tmp"))
}

func TmpFileAssignHelper(whichReduce int, tmpFileDirectoryName string) []string {
	var res []string
	path, _ := os.Getwd()
	rd, _ := ioutil.ReadDir(path + "/" + tmpFileDirectoryName)
	for _, fi := range rd {
		if strings.HasPrefix(fi.Name(), "mr-tmp") && strings.HasSuffix(fi.Name(), strconv.Itoa(whichReduce)) {
			res = append(res, fi.Name())
		}
	}
	return res
}
