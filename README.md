## 1.实验准备

MapReduce论文阅读

配置golang

git clone项目

## 2.任务梳理

### 2.1 描述
实现分布式mr,一个coordinator,一个worker（启动多个）,在这次实验都在一个机器上运行。worker通过rpc和coordinator交互。worker请求任务,进行运算,写出结果到文件。coordinator需要关心worker的任务是否完成，在超时情况下将任务重新分配给别的worker

### 2.2 规则

- map阶段需要将中间keys分成nReduce个数, nReduce通过main/mrcoordinator.go传给MakeCoordinator()

- worker需要将第X个reduce task结果放到mr-out-X中。

- mr-out-X要一行一行生成,kv形式。main/mrsequential.go中有，拿来就完事了

- main/mrcoordinator.go从mr/coordinator.go 的 Done()方法得知任务完成并关闭自己。

- 任务都完成后，worker也得关闭

### 2.3 提示

- 一开始可以从mr/worker.go的 Worker()方法做，发送rpc给coordinator请求任务，然后coordinator分配任务，然后worker读文件并且map函数处理。

- map reduce函数都是通过go插件装载 (.so文件)

- mr/ 文件变了就需要重新build

- 都在一个文件系统,worker天然实现文件共享，先凑合着起步

- 中间文件命名 mr-X-Y X是map任务号，y是reduce任务号

- worker的map方法用json存储中间kv对，reduce再读回来，因为真正分布式worker都不在一个机器上，涉及网络传输，所以用json安排上。

- worker的map可以用 worker.go里面的ihash(key)得到特定key的reduce任务号

- mrsequential.go 代码可以借鉴

- coordinator里面的共享数据需要加锁

- worker有时候需要等待
  (Workers will sometimes need to wait, e.g. reduces can't start until the last map has finished. One possibility is for workers to periodically ask the coordinator for work, sleeping with time.Sleep() between each request. Another possibility is for the relevant RPC handler in the coordinator to have a loop that waits, either with time.Sleep() or sync.Cond. Go runs the handler for each RPC in its own thread, so the fact that one handler is waiting won't prevent the coordinator from processing other RPCs.)


- 如果任务重试机制，记得不要生成重复任务

- mrapps/crash.go 随机干掉map reduce，可以用来测试恢复功能

- 确保没有人在出现崩溃时观察部分写入的文件，用ioutil.TempFile创建临时文件，用os.Rename重命名

### 3.配置goland调试环境踩坑


### 4.项目记录

1.rpc交互，写worker到master请求任务。首先看看是怎么联通的，原来是worker通过反射机制，传入call,&request(ExampleArgs),&response(ExampleReply)
